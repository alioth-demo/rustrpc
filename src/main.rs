use tonic::{Request, Response, Status};
use async_trait::async_trait;

pub mod people {
    tonic::include_proto!("people");
}

#[derive(Default)]
struct MyHeartbeat {}

#[async_trait]
impl people::beater_server::Beater for MyHeartbeat {
    async fn heartbeating(&self, request: Request<people::Heartbeat>) -> Result<Response<people::Heartbeat>, Status> {
        println!("received {:?}", &request);
        let mut ret = people::Heartbeat::default();
        ret.unit_number = 25;
        Ok(Response::new(ret))
    }
}

#[tokio::main]
async fn main() -> std::io::Result<()> {
    let server = tokio::spawn(async move {
        server_main().await;
    });
    let client = tokio::spawn(async move {
        client_main(10).await;
        client_main(20).await;
        client_main(30).await;
    });
    match tokio::try_join!(server, client) {
        Ok(..) => println!("complete"),
        Err(e) => eprintln!("{}", e),
    }
    Ok(())
}

async fn server_main() {
    let addr = "[::1]:50051".parse().unwrap();
    let hb = MyHeartbeat::default();
    let _ukwn = tonic::transport::Server::builder()
        .add_service(people::beater_server::BeaterServer::new(hb))
        .serve(addr)
        .await;
}

async fn client_main(unit: u32) {
    if let Ok(mut client) = people::beater_client::BeaterClient::connect("http://[::1]:50051").await {
        let request = tonic::Request::new(people::Heartbeat {
            unit_number: unit,
            is_client: true,
        });
        if let Ok(response) = client.heartbeating(request).await {
            println!("{:?}", response)
        } else {
            println!("failed inner")
        }
    } else {
        println!("failed outer")
    }
}
